import PageLayout from "./components/pageLayout/PageLayout";
import {
  AuthenticatedTemplate,
  UnauthenticatedTemplate,
} from "@azure/msal-react";
import ProfileContent from "./components/ProfileContent/ProfileContent";

function App() {
  return (
    <PageLayout>
      <AuthenticatedTemplate>
        <ProfileContent />
      </AuthenticatedTemplate>
      <UnauthenticatedTemplate>
        <p>Please sign in</p>
      </UnauthenticatedTemplate>
    </PageLayout>
  );
}

export default App;
