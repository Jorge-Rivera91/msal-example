import { useIsAuthenticated } from "@azure/msal-react";
import { Navbar } from "react-bootstrap";
import SignInButton from "../SignInButton";
import SignOutButton from "../SignOutButton";

const PageLayout = (props) => {
  const isAuthenticated = useIsAuthenticated();

  return (
    <>
      <Navbar bg='primary' variant='dark'>
        <a className='navbar-brand' href='/'>
          MSAL React Tutorial
        </a>
        {isAuthenticated ? <SignOutButton /> : <SignInButton />}
      </Navbar>

      <h5>
        <center>
          Welcome to the Microsoft Authentication Library For React Tutorial
        </center>
      </h5>
      <br />
      <br />
      {props.children}
    </>
  );
};

export default PageLayout;
