import { useMsal } from "@azure/msal-react";
import { useState } from "react";
import { Button } from "react-bootstrap";
import { loginRequest } from "../../config/authConfig";
import ProfileData from "../ProfileData";
import { callMsGraph } from "../../config/graph";

const ProfileContent = () => {
  const { instance, accounts } = useMsal();
  const [graphData, setGraphData] = useState(null);
  const name = accounts[0] && accounts[0].name;

  const requestAccessToken = () => {
    const request = {
      ...loginRequest, // just has the scope
      account: accounts[0],
    };

    instance
      .acquireTokenSilent(request)
      .then((response) => {
        // setAccessToken(response.accessToken);
        callMsGraph(response.accessToken).then((response) =>
          setGraphData(response)
        );
      })
      .catch(() => {
        instance
          .acquireTokenPopup(request)
          .then((response) => {
            // setAccessToken(response.accessToken);
            callMsGraph(response.accessToken).then((response) =>
              setGraphData(response)
            );
          })
          .catch((e) => console.log(e.message));
      });
  };

  return (
    <>
      <h5 className='card-title'>Welcome {name}</h5>
      {graphData ? (
        <ProfileData graphData={graphData} />
      ) : (
        <Button variant='secondary' onClick={requestAccessToken}>
          Request Access Token
        </Button>
      )}
    </>
  );
};

export default ProfileContent;
