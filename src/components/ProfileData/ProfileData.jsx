const ProfileData = (props) => {
  return (
    <div id='profile-div'>
      <p>
        <strong>First Name:</strong>
        {props.graphData.givenName}
      </p>
      <p>
        <strong>Last Name:</strong>
        {props.graphData.surname}
      </p>
      <p>
        <strong>Email:</strong>
        {props.graphData.userPrincipalName}
      </p>
      <p>
        <strong>id:</strong>
        {props.graphData.id}
      </p>
    </div>
  );
};

export default ProfileData;
