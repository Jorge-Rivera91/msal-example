import { useMsal } from "@azure/msal-react";
import { Button } from "react-bootstrap";

const handleSignOut = (instance) => {
  // instance.logoutRedirect().catch((e) => console.log(e.message));
  instance.logoutPopup().catch((e) => console.log(e.message));
};

const SignOutButton = () => {
  const { instance } = useMsal();

  return (
    <Button
      variant='secondary'
      className='ml-auto'
      onClick={() => {
        handleSignOut(instance);
      }}
    >
      Sign out using Popup
    </Button>
  );
};

export default SignOutButton;
