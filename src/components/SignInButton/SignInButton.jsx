import { useMsal } from "@azure/msal-react";
import { Button } from "react-bootstrap";
import { loginRequest } from "../../config/authConfig";

const handleLogin = (instance) => {
  // instance.loginRedirect(loginRequest).catch(e=> { console.log(e.message) })
  instance.loginPopup(loginRequest).catch((e) => {
    console.log(e.message);
  });
};

const SignInButton = () => {
  const { instance } = useMsal();
  return (
    <Button
      variant='secondary'
      className='ml-auto'
      onClick={() => {
        handleLogin(instance);
      }}
    >
      Sign in usign Popup
    </Button>
  );
};

export default SignInButton;
