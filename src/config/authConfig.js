export const msalConfig = {
  auth: {
    clientId: "bd03499a-fb47-44f4-87c7-91f91a6198e2",
    authority:
      "https://login.microsoftonline.com/1b311de7-aed3-4c8b-b468-706a4985d50a",
    redirectUri: "http://localhost:3000/",
  },
  cache: {
    cacheLocation: "sessionStorage",
    storeAuthStateInCookie: false, // Set this to "true" if you are having issues on IE11 or Edge
  },
};

export const loginRequest = {
  scopes: ["User.Read"],
};

// Add the endpoints here for Microsoft Graph API services you'd like to use.
export const graphConfig = {
  graphMeEndpoint: "https://graph.microsoft.com/v1.0/me",
};
